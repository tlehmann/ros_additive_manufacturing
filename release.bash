#!/bin/bash
source <(curl -s https://gitlab.com/VictorLamoine/bloom_local_release/raw/master/bloom_local_release.bash)

os=ubuntu
os_version=bionic
ros_distro=melodic
rosdep_yaml_name="ros_additive_manufacturing"
rosdep_list_name="31-ros-additive-manufacturing"

# Add rosdep rules we will need later when installing debian packages
append_rosdep_key ram-msgs
append_rosdep_key ram-utils
append_rosdep_key ram-modify-trajectory
append_rosdep_key ram-display
append_rosdep_key ram-post-processor
append_rosdep_key ram-path-planning
append_rosdep_key ram-trajectory
rosdep update

# Generate debians and install them
generate_deb ram-documentation
install_deb ram-documentation

generate_deb ram-msgs
install_deb ram-msgs

generate_deb ram-utils "" true # VTK CPack debian misses some information
install_deb ram-utils

generate_deb ram-modify-trajectory
install_deb ram-modify-trajectory

generate_deb ram-display
install_deb ram-display

generate_deb ram-post-processor
install_deb ram-post-processor

generate_deb ram-trajectory
install_deb ram-trajectory

generate_deb ram-path-planning "" true # VTK CPack debian misses some information
install_deb ram-path-planning

generate_deb ram_qt_guis "" true # VTK CPack debian misses some information
install_deb ram-qt-guis

clear_rosdep_keys
zip -r ros_additive_manufacturing *.deb install.bash
rm *.deb *.ddeb
installed_deb_info
